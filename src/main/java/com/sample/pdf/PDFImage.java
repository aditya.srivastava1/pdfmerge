package com.sample.pdf;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.printing.Scaling;
import org.apache.pdfbox.util.Matrix;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.logging.Logger;

public class PDFImage {

    Logger logger = Logger.getLogger(this.getClass().getName());

    public void addImage(String pdfpath, String imagePath, String output){

        float height = PDRectangle.A4.getHeight();
        float width = PDRectangle.A4.getWidth();

        logger.info("Height px: " + height + " Width px: "+width);

        long startTime = Instant.now().toEpochMilli();

        File file = new File(pdfpath);

        try (PDDocument doc = PDDocument.load(file)){

            logger.info("Add image to pdf started");

            PDImageXObject pdImage = PDImageXObject.createFromFile(imagePath, doc);
            doc.addPage(new PDPage());
            PDPage page = doc.getPage(doc.getNumberOfPages()-1);

            PDPageContentStream contentStream = new PDPageContentStream(doc,page, true, false);

            Integer[] scaledDimension = getScaledDimension(pdImage.getWidth(), pdImage.getHeight(),((int) width - 100), ((int) height - 100));

            logger.info("---> Height px: " + scaledDimension[1] + " Width px: "+scaledDimension[0]);

            int x = (int) (PDRectangle.A4.getWidth() - scaledDimension[0]) /2 ;
            int y = (int) (PDRectangle.A4.getHeight() - scaledDimension[1]) / 2;

            contentStream.drawImage(pdImage, 10, y, scaledDimension[0], scaledDimension[1] );
            contentStream.close();
            doc.save(output);

            logger.info("Add image to pdf ended");
        } catch (IOException e) {
            e.printStackTrace();
        }

        long endTime = Instant.now().toEpochMilli();

        long timeElapsed = endTime - startTime;

        logger.info("Execution time in milliseconds : " + timeElapsed);

    }

    public Integer[] getScaledDimension(int imageWidth, int imageHeight, int gridWidth, int gridHeight) {

        int original_width = imageWidth;
        int original_height = imageHeight;
        int bound_width = gridWidth;
        int bound_height = gridHeight;

        int new_width = original_width;
        int new_height = original_height;

        if (original_width > bound_width) {
            new_width = bound_width;
            new_height = (new_width * original_height) / original_width;
        }

        if (new_height > bound_height) {
            new_height = bound_height;
            new_width = (new_height * original_width) / original_height;
        }

        return new Integer[]{new_width, new_height};
    }

}
